module gitlab.com/qrhelper/qrrouter

go 1.15

require (
	github.com/allegro/bigcache v1.1.0
	github.com/go-ini/ini v1.62.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/mux v1.7.0
	github.com/gorilla/sessions v1.1.3
	github.com/gorilla/websocket v1.4.1-0.20190205004414-7c8e298727d1 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/mailgun/timetools v0.0.0-20170619190023-f3a7b8ffff47 // indirect
	github.com/minio/minio-go v6.0.14+incompatible
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/smartystreets/goconvey v1.7.2 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/vulcand/oxy v0.0.0-20181130145254-c34b0c501e43
	gitlab.com/qrhelper/qrhelperdatastructs v0.0.0-20180807181100-02472464ee1a
	go.mongodb.org/mongo-driver v1.5.3
	golang.org/x/sys v0.0.0-20200831180312-196b9ba8737a // indirect
	gopkg.in/ini.v1 v1.41.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
)
