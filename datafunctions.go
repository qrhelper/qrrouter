package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
	datastructs "gitlab.com/qrhelper/qrhelperdatastructs"
)

// // FetchResource - fetch resource
// func (db *gorm.DB) FetchResource(resourceid string) datastructs.Resource {

// 	// works because destination struct is passed in
// 	resource := datastructs.Resource{}
// 	db.Table("resources").First(&resource)
// 	return resource

// }
func FetchResource(db *gorm.DB, resource string) (datastructs.Resource, error) {

	res := datastructs.Resource{}
	err := db.Model("resource").First(&res).Error
	if err != nil {
		return res, err
	}
	err = storeCacheResource(res)
	if err != nil {
		log.Println("Error storing resource in cache", err)
	}
	return res, nil
}

func storeCacheResource(result datastructs.Resource) error {

	b, err := json.Marshal(result)
	if err != nil {
		fmt.Println(err)
		return err
	}

	err = cache.Set(result.Uuid, []byte(b))

	if err != nil {
		log.Println("error storing resource", err)
		return err
	}
	return err

}

func getCacheResource(resourceName string) (datastructs.Resource, error) {

	cacheres := datastructs.Resource{}
	res, errres := cache.Get(resourceName)
	if res != nil && errres != nil {
		err := json.Unmarshal([]byte(res), &cacheres)
		if err != nil {
			log.Println("error unmarshalling ", err)
		}
		return cacheres, errres
	}

	return cacheres, errres

}
