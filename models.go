package main

import (
	"time"

	"github.com/jinzhu/gorm"
)

var Resources []Resource = []Resource{
	Resource{
		Model:       gorm.Model{},
		Uuid:        "7f56ea17-caa6-4dc8-a2e3-7616fde9c649",
		OrgId:       "23",
		CreatedTime: time.Now(),
		Description: "234",
		Email:       "",
		Name:        "",
		Action:      "forward",
		Address:     "https://catfact.ninja/",
		AccessCount: 0,
	},
	Resource{
		Model:       gorm.Model{},
		Uuid:        "7f56ea17-caa6-4dc8-a2e3-7616fde9c647",
		OrgId:       "24",
		CreatedTime: time.Now(),
		Description: "23e4",
		Email:       "",
		Name:        "",
		Action:      "forward",
		Address:     "https://catfacts.co/",
		AccessCount: 0,
	},
}

type Resource struct {
	gorm.Model
	Uuid        string `gorm:"type:char(36);primary_key"`
	OrgId       string
	CreatedTime time.Time
	Description string
	Email       string
	Name        string
	Action      string
	Address     string
	AccessCount int64
}
