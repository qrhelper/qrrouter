package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func crud(db *gorm.DB) {
	fmt.Println("============================")
	fmt.Println("CRUD")
	fmt.Println("============================")

	db.CreateTable(&Resource{})

	// Seeding db using custom model data
	for _, resource := range Resources {
		db.Create(&resource)
	}

}
