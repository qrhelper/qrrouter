package main

import (
	"fmt"
	"os"
	"testing"

	datastructs "gitlab.com/qrhelper/qrhelperdatastructs"
)

var invokeCount int

var resource1 = datastructs.Resource{Uuid: "333c1dd2-d454-11e6-a110-b34e9f2c654a", Description: "yahoo", Action: "redirect", Address: "https://www.yahoo.com", AccessCount: 0}
var resource2 = datastructs.Resource{Uuid: "444edd7c-d454-11e6-92b9-374c2fc3d623", Description: "yahoo", Action: "redirect", Address: "https://www.google.com", AccessCount: 0}
var resource3 = datastructs.Resource{Uuid: "444edd7c-d454-11e6-92b9-374c2fc3d624", Description: "redirect", Action: "proxy", Address: "/test", AccessCount: 0}
var resource4 = datastructs.Resource{Uuid: "444edd7c-d454-11e6-92b9-374c2fc3d625", Description: "forward", Action: "redirect", Address: "/test", AccessCount: 0}
var resource5 = datastructs.Resource{Uuid: "444edd7c-d454-11e6-92b9-374c2fc3d626", Description: "S3 redirect", Action: "s3serve", Address: "s3serve", AccessCount: 0}
var resource6 = datastructs.Resource{Uuid: "444edd7c-d454-11e6-92b9-374c2fc3d627", Description: "S3 forward", Action: "s3redirect", Address: "s3redirect", AccessCount: 0}

func init() {
	VARFetchResourceMongo = func(resourceid string) datastructs.Resource {
		if resourceid == "333c1dd2-d454-11e6-a110-b34e9f2c654a" {
			return resource1
		} else if resourceid == "444edd7c-d454-11e6-92b9-374c2fc3d623" {
			return resource2
		} else if resourceid == "444edd7c-d454-11e6-92b9-374c2fc3d624" {
			return resource3
		} else if resourceid == "444edd7c-d454-11e6-92b9-374c2fc3d625" {
			return resource4
		} else if resourceid == "444edd7c-d454-11e6-92b9-374c2fc3d626" {
			return resource5
		} else if resourceid == "444edd7c-d454-11e6-92b9-374c2fc3d627" {
			return resource6
		}

		return resource6
	}
	return
}

// func insertFixtures() {
// 	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
// 	defer cancel()
// 	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
// 	if err != nil {
// 		log.Println("Unable to get ")
// 		log.Println(err)
// 	}
// 	collection := client.Database("res").Collection("res")

//		ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
//		defer cancel()
//		res, err := collection.InsertOne(ctx, bson.D{{"name", "pi"}, {"value", 3.14159}})
//		id := res.InsertedID
//		fmt.Println("getting ID")
//		fmt.Println(id)
//		// if err := collection.InsertOne(ctx, resource1); err != nil {
//		// 	log.Fatal("Unable to insert test record resource1")
//		// }
//		// if err := MgoSession.DB(MongoDBDatabase).C("res").Insert(resource2); err != nil {
//		// 	log.Fatal("Unable to insert test record resource2")
//		// }
//		// if err := MgoSession.DB(MongoDBDatabase).C("res").Insert(resource3); err != nil {
//		// 	log.Fatal("Unable to insert test record resource3")
//		// }
//		// if err := MgoSession.DB(MongoDBDatabase).C("res").Insert(resource4); err != nil {
//		// 	log.Fatal("Unable to insert test record resource4")
//		// }
//		// if err := MgoSession.DB(MongoDBDatabase).C("res").Insert(resource5); err != nil {
//		// 	log.Fatal("Unable to insert test record resource4")
//		// }
//		// if err := MgoSession.DB(MongoDBDatabase).C("res").Insert(resource6); err != nil {
//		// 	log.Fatal("Unable to insert test record resource4")
//		// }
//	}
func insertFixturesCache() {
	cache.Set("my-unique-key", []byte("value"))
}

func TestMain(m *testing.M) {
	//var cache bigcache

	//client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	// err != nil {
	// 	log.Fatalf(err)
	// }
	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	// if err := pool.Retry(func() {
	// 	var err error
	// 	//db, err = mgo.Dial(fmt.Sprintf("localhost:%s", resource.GetPort("27017/tcp")))
	// 	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	// 	defer cancel()
	// 	if err != nil {
	// 		return err
	// 	}

	// }); err != nil {
	// 	log.Fatalf("Could not connect to docker: %s", err)
	// }
	//insertFixtures()
	insertFixturesCache()
	// // When you're done, kill and remove the container

	// Run the test suite

	fmt.Println("m.Run - start")
	retCode := m.Run()
	fmt.Println("m.Run - End")
	// Make sure we DropDatabase so we make absolutely sure nothing is left or locked while wiping the data and
	// close session

	fmt.Println("exiting test main")
	// call with result of m.Run()
	os.Exit(retCode)

}
