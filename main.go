package main

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/allegro/bigcache"
	"github.com/joho/godotenv"

	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/sessions"
)

var COOKY_SECRET []byte
var LOAD_DB bool

// CookieStore - the cookie store/encrypt phrase
var CookieStore = sessions.NewCookieStore(COOKY_SECRET)

// AwsURL - Amazon AWS url
var AwsURL = os.Getenv("AWS_URL")

// AwsKey - Amazon AWS key
var AwsKey = os.Getenv("AWS_KEY")

// AwsPassPhrase - Amazon AWS passphrase
var AwsPassPhrase = os.Getenv("AWS_PASSPHRASE")

// AwsBucket - Amazon AWS Bucket
var AwsBucket = os.Getenv("AWS_BUCKET")

var cache, _ = bigcache.NewBigCache(bigcache.DefaultConfig(180 * time.Minute))
var DB = Connect()

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
	COOKY_SECRET = []byte(os.Getenv("COOKY_SECRET"))
	LOAD_DB, err = strconv.ParseBool(os.Getenv("LOAD_DB"))
	if err != nil {
		log.Println("error getting load_db env variable")
	}
	//cache.NewBigCache(bigcache.DefaultConfig(180 * time.Minute))

	if err != nil {
		log.Println("Error loading database")
	}
	//var cache, _ = bigcache.NewBigCache(bigcache.DefaultConfig(180 * time.Minute))

}

func main() {

	//db := Connect()
	log.Println(LOAD_DB)
	if LOAD_DB == true {
		crud(DB)
	}

	defer DB.Close()

	router := NewRouter()
	var portnumber = ":" + os.Getenv("QRROUTER_PORT")
	log.Println("Port number is ", portnumber)
	// Set up CORS
	corsObj := handlers.AllowedOrigins([]string{"*"})
	// and use combined logging handler as well
	log.Fatal(http.ListenAndServe(portnumber, handlers.CombinedLoggingHandler(os.Stdout, handlers.CORS(corsObj)(router))))
}
