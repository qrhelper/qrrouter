package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/qrhelper/qrhelperdatastructs"
)

var testresource = datastructs.Resource{Uuid: "333444d2-d454-11e6-a110-b34e9f2c654a", Description: "yahoo", Action: "redirect", Address: "https://www.yahoo.com", AccessCount: 0}

func TestFetchResource(t *testing.T) {
	t.Log("TestFetchResource Test")

	testResult := FetchResource("444edd7c-d454-11e6-92b9-374c2fc3d623")
	if testResource(testResult, resource2) == false {
		t.Log("feched resource is not same as inserted resource")
		t.Fail()
	}

}

func testResource(r1 datastructs.Resource, r2 datastructs.Resource) bool {
	var resflag bool
	if r1.Action == r2.Action &&
		r1.Address == r2.Address &&
		r1.Description == r2.Description &&
		r1.Uuid == r2.Uuid {
		resflag = true
	} else {
		resflag = false
	}

	return resflag

}

func TestStoreCache(t *testing.T) {
	t.Log("testStoreCache Test")

	err := storeCacheResource(testresource)
	if err != nil {
		t.Log(err)
		//t.Fail("Unable to load cache")
	}
	entry2, _ := cache.Get("333444d2-d454-11e6-a110-b34e9f2c654a")
	assert.ObjectsAreEqual(entry2, testresource)

	//assert.Equal(t, entry2, testresource)
}
