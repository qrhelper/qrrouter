package main

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/allegro/bigcache"
	"github.com/stretchr/testify/assert"
	datastructs "gitlab.com/qrhelper/qrhelperdatastructs"
)

var res1 = datastructs.Resource{Uuid: "333c1dd2-d454-11e6-a110-b34e9f2c654a", Description: "yahoo", Action: "redirect", Address: "https://www.yahoo.com", AccessCount: 0}

func TestCache(t *testing.T) {
	cache, _ := bigcache.NewBigCache(bigcache.DefaultConfig(180 * time.Minute))
	s2, _ := json.Marshal(res1)
	cache.Set("22", []byte(s2))
	entry2, _ := cache.Get("22")
	a := datastructs.Resource{}
	err := json.Unmarshal([]byte(entry2), &a)
	if err != nil {
		t.Log("error unmarshalling")
	}

	fmt.Println(reflect.DeepEqual(a, res1))
	//t.Log("TestGetUUID1 code", w.Code)
	assert.Equal(t, a, res1)

}
