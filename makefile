BINARY_NAME=main.out
 
build:
	go build -o ${BINARY_NAME} `ls *.go | grep -v _test.go`
 
run: build
	./${BINARY_NAME}
 
run: test
	go test *.go	
clean:
	go clean
	rm ${BINARY_NAME}
